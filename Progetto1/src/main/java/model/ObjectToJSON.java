package model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
 
import model.Data;
import model.Persona;
 
public class ObjectToJSON {
    public static void main(String[] args) {
        Persona persona = new Persona("Mario", "Rossi", new Data(1, 1, 1980));
       
        Gson gson = new Gson();
        String jsonString = gson.toJson(persona);
        System.out.println(jsonString);
        Persona object = gson.fromJson(jsonString, Persona.class);
        System.out.println(object);
    }
}
