package model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import model.Data;
import model.Persona;

public class ObectsToJSON {
	public static void main(String[] args) {
		Persona persona1 = new Persona("Mario", "Rossi", new Data(1, 1, 1980));
		Persona persona2 = new Persona("Giuseppe", "Verdi", new Data(10, 10, 1970));
		ArrayList list = new ArrayList();
		list.add(persona1);
		list.add(persona2);
		
		Gson gson = new Gson();
		String jsonString = gson.toJson(list);
		
		System.out.println(jsonString);
		System.out.println("\n");
		java.lang.reflect.Type listType = new TypeToken<ArrayList<Persona>>() {
		}.getType();
		System.out.println(gson.fromJson(jsonString, listType).toString());
	}
}
